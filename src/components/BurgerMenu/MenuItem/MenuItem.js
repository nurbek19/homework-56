import React from 'react';

const MenuItem = props => {
    return (
        <li>
            <span>{props.name}</span>
            <button onClick={props.lessItem} disabled={props.disabled}>LESS</button>
            <button onClick={props.moreItem}>MORE</button>
        </li>
    )
};

export default MenuItem;