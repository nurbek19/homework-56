import React, {Component} from 'react';
import BurgerMenu from './components/BurgerMenu/BurgerMenu';
import BurgerTop from './components/BurgerTop/BurgerTop';
import BurgerBottom from './components/BurgerBottom/BurgerBottom';
import Ingredient from './components/Ingredient/Ingredient';

import './App.css';

class App extends Component {
    state = {
        ingredients: {
            Salad: 0,
            Bacon: 0,
            Cheese: 0,
            Meat: 0
        },
        prices: {
            Salad: 5,
            Cheese: 20,
            Bacon: 30,
            Meat: 50
        },

        disabled: {
            Salad: true,
            Cheese: true,
            Bacon: true,
            Meat: true
        },
        total: 20
    };

    moreItem = (name) => {
        const ingredients = {...this.state.ingredients};
        ingredients[name]++;

        const disabled = {...this.state.disabled};
        disabled[name] = false;

        let total = this.state.total + this.state.prices[name];

        this.setState({ingredients, total, disabled})
    };

    lessItem = (name) => {
        const ingredients = {...this.state.ingredients};
        const disabled = {...this.state.disabled};
        let total = this.state.total;

        if (ingredients[name] > 0) {
            ingredients[name]--;
            total -= this.state.prices[name];
        }

        if (ingredients[name] === 0) {
            disabled[name] = true;
        }

        this.setState({ingredients, total, disabled});
    };

    render() {
        const ingredientsArray = [];
        const ingredientsNames = Object.keys(this.state.ingredients);
        const ingredientsAmounts = Object.values(this.state.ingredients);
        let index = 0;

        for (let i = 0; i < ingredientsNames.length; i++) {
            let ingredientName = ingredientsNames[i];
            let ingredientAmount = ingredientsAmounts[i];

            for (let j = 0; j < ingredientAmount; j++) {
                index++;
                ingredientsArray.push(<Ingredient ingredient={ingredientName} key={index}/>);
            }
        }

        return (
            <div className="App">
                <div className="Burger">
                    <BurgerTop/>
                    {ingredientsArray}
                    <BurgerBottom/>
                </div>
                <BurgerMenu
                    status={this.state.disabled}
                    totalSum={this.state.total}
                    ingredients={this.state.ingredients}
                    moreItem={this.moreItem}
                    lessItem={this.lessItem}
                />
            </div>
        );
    }
}

export default App;
